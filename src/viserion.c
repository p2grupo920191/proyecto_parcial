#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear_Viserion(void *ref, size_t tamano){
    Viserion *buf = (Viserion *)ref;
    buf->a = 12;
    int i = 0;
    buf-> status=1;
    for(i = 0; i< 100; i++){
    	buf->b[i] = 2*i;
    }
    buf->msg = (char *)malloc(sizeof(char)*1000);
}

void destruir_Viserion(void *ref, size_t tamano){
    Viserion *buf = (Viserion *)ref;
    buf->a = 0;
    buf->status=0;
    free(buf->msg);
    for(int i = 0; i< 100; i++){
    	buf->b[i] = 0;
    }
    free(buf->msg);
}

