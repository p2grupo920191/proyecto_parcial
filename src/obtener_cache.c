#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"
#include "slaballoc.h"


//devolver el puntero ptr_data y marcar el slab como EN_USO, si todos los slabs estan en uso y bandera crecer es igual a 1, su allocator debe incrementar el numero de slabs al dobre del tamano anterior.
void *obtener_cache(SlabAlloc *alloc, int crecer){
   Slab *sl=alloc->slab_ptr;
   void *ptdat=NULL;
   int i;
   if(crecer==1 && ((alloc->cantidad_en_uso) == (alloc->tamano_cache))){
	//Copia de PTR_DATA a PTR_OLD previo a realloc
	for (i=0;i< alloc->tamano_cache;i++){
		(sl+(i*(sizeof(Slab))))->ptr_data_old = (sl+(i*(sizeof(Slab))))->ptr_data;
		}
	//REALLOC
	sl = realloc(alloc->slab_ptr,2*(alloc->tamano_cache)*(sizeof(Slab)));
	alloc->mem_ptr = realloc(alloc->mem_ptr,2*(alloc->tamano_cache)*(alloc->tamano_objeto));
	
	for(i=0; i< alloc->tamano_cache; i++){	//For para asegurarnos que los primeros Slabs estan en uso
		(sl+(i*(sizeof(Slab))))->status = EN_USO;
		
	}
	
	for(i=0; i< alloc->tamano_cache; i++){
	   (sl+(i*sizeof(Slab)))->ptr_data = ((alloc->mem_ptr)+(i*(alloc->tamano_objeto)));
	 
	}
	alloc->tamano_cache = 2*(alloc->tamano_cache);

   }
   	//En el caso de tener disponibilidad en cache
   if ((alloc->cantidad_en_uso) < (alloc->tamano_cache)){
	   for(i=0;i < alloc->tamano_cache;i++){
		if((sl+(i*sizeof(Slab)))->status == 0){
			ptdat = (sl+(i*sizeof(Slab)))->ptr_data;
			alloc->cantidad_en_uso++;
			(sl+(i*sizeof(Slab)))->status = EN_USO;
			//break;
			return ptdat;
		}
	    }
   }


   return ptdat;
}

