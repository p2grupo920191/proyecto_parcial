#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear_Drogon(void *ref, size_t tamano){
    Drogon *buf = (Drogon *)ref;
    buf->l = 5;
    int j = 0;
    for(j = 0; j< 100; j++){
    	buf->p[j] = j;
    }
    buf->msg = (char *)malloc(sizeof(char)*1000);
    buf->status=1.0f;
}

void destruir_Drogon(void *ref, size_t tamano){
    Drogon *buf = (Drogon *)ref;
    buf->l = 0;
    int j = 0;
    for(j = 0; j< 100; j++){
    	buf->p[j] = 0;
    }
    buf->status=0.0f;
    free(buf->msg);

}

