#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"
#include "slaballoc.h"

void stats_cache(SlabAlloc *cache){
printf("Nombre de cache:\t%s\n",cache->nombre);
printf("Cantidad de slabs:\t%d\n",cache->tamano_cache);
printf("Cantidad de slabs en uso:\t%d\n",cache->cantidad_en_uso);
printf("Tamano de objeto:\t%ld\n",cache->tamano_objeto);
printf("Direccion de cache->mem_ptr:\t%p\n",cache->mem_ptr);
Slab *sl =cache->slab_ptr;
for(int i=0;i<cache->tamano_cache;i++){
   if(((sl+(i*sizeof(Slab)))->status)==0){
      printf("Direccion ptr[%d].ptr_data:\t%p\tDISPONIBLE\t  ptr_data_old\t%p\n",i,((sl->ptr_data)+(i*(cache->tamano_objeto))),((sl->ptr_data)+(i*(cache->tamano_objeto))));
   }else{
      printf("Direccion ptr[%d].ptr_data:\t%p\tEN_USO\t  ptr_data_old\t%p\n",i,((sl->ptr_data)+(i*(cache->tamano_objeto))),((sl->ptr_data)+(i*(cache->tamano_objeto))));}

}




}
