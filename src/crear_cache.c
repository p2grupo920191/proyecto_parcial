#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"
#include "slaballoc.h"

SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
   SlabAlloc *alloc=malloc(tamano_objeto*TAMANO_CACHE);	//Region memoria de SlabAlloc
   void *mem=malloc(tamano_objeto*TAMANO_CACHE);	//Region memoria objetos
   Slab *arr_sl=malloc(sizeof(Slab)*TAMANO_CACHE);	//Region memoria Slabs
   //ASIGNACION DE DATOS AL ALLOCATOR
   alloc->nombre=nombre;
   alloc->mem_ptr = mem;
   alloc->tamano_cache = TAMANO_CACHE;
   alloc->cantidad_en_uso = 0;
   alloc->tamano_objeto = tamano_objeto;
   alloc->constructor = constructor;
   alloc->destructor = destructor;
   alloc->slab_ptr=arr_sl;
   
   for(int i=0;i<TAMANO_CACHE;i++){
     (arr_sl+(i*sizeof(Slab)))->ptr_data = mem+(i*tamano_objeto);	//Inicializamos los Slabs
     (arr_sl+(i*sizeof(Slab)))->status = DISPONIBLE;				//Inicializamos el status de cada Slab
     constructor((mem+(i*tamano_objeto)),tamano_objeto);		//Inicializamos los Objetos
   }
   

   return alloc;
}
