#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"
#include "slaballoc.h"


//retorno de tipo objeto SlabAlloc
SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
   void *mem=malloc(tamano_objeto*TAMANO_CACHE);
    //falta inicializar
   struct SlabAlloc alloc;
   alloc.nombre=nombre;
   alloc.mem_ptr = mem;
   alloc.tamano_cache = TAMANO_CACHE;
   alloc.cantidad_en_uso = 0;
   alloc.tamano_objeto = tamano_objeto;
   alloc.constructor_fn = constructor;
   alloc.destructor_fn = destructor;
   struct SlabALloc *alloc_pointer=malloc(tamano_objeto*TAMANO_CACHE);
   //crear objetos
   for(int i=0;i<TAMANO_CACHE;i++){
      constructor((mem+i),alloc->tamano_objeto);
      
   }
   alloc_pointer=&alloc;
   printf("crear_cache");//borrar
return alloc_pointer;
}

//llamar funcion destructor de cada objeto y luego liberar toda memoria asignada
void destruir_cache(SlabAlloc *cache){
  if(cache->cantidad_en_uso==0){
  for (int i=0;i<cache->tamano_cache;i++){
     //destruir cada objeto
  }
  free(cache->mem_ptr);}
  printf("destruir_cache");//borrar

}

//devolver el puntero ptr_data y marcar el slab como EN_USO, si todos los slabs estan en uso y bandera crecer es igual a 1, su allocator debe incrementar el numero de slabs al dobre del tamano anterior.
void *obtener_cache(SlabAlloc *alloc, int crecer){
   Slab *sl=alloc->slab_ptr;
   SlabAlloc *newalloc;
   void *ptr_dato;
   if (alloc->cantidad_en_uso==alloc->tamano_objeto && crecer==1){
      newalloc=realloc(alloc,2*alloc->tamano_cache);
	//falta inicializar los nuevos slabs
   }
   void *ptdat=NULL;
   //falta recorrer el newAlloc, cambiar la posicion de los slabs y guardar la    		anterior posicion de los slabs.
   alloc=newalloc;	
   for (int i=0;i<alloc->tamano_cache;i++){
      int s=(sl+i)->status;
      if(s==0){
	 (sl+i)->status=1;
         alloc->cantidad_en_uso++;
         ptr_dato=(sl+i)->ptr_data;
	 ptdat=ptr_dato;
         printf("obtener_cache");//borrar
         break;
      }
   }
   return ptdat;
   
}

//busca el slab asociado al objeto y lo marca como DISPONIBLE
void devolver_cache(SlabAlloc *alloc, void *obj){
   Slab *arr_sl=alloc->slab_ptr;
   for(int i=0;i<alloc->tamano_cache;i++){
      if((arr_sl+i)->ptr_data==obj){
         arr_sl->status=0;
	 printf("devolver_cache");//borrar
	 alloc->destructor(obj,alloc->tamano_objeto);
      }
   }

}



