
bin/prueba: obj/prueba.o obj/Ejemplo.o obj/drogon.o obj/viserion.o obj/rhaegal.o obj/crear_cache.o obj/destruir_cache.o obj/devolver_cache.o obj/obtener_cache.o obj/stats_cache.o
	gcc obj/prueba.o obj/Ejemplo.o obj/drogon.o obj/viserion.o obj/rhaegal.o  obj/crear_cache.o obj/destruir_cache.o obj/devolver_cache.o obj/obtener_cache.o obj/stats_cache.o -o bin/prueba		#agregue los archivos .o que necesite

obj/prueba.o: src/prueba.c
	gcc -Wall -c -I include/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -c -I include/ src/Ejemplo.c -o obj/Ejemplo.o

obj/drogon.o: src/drogon.c
	gcc -Wall -c -I include/ src/drogon.c -o obj/drogon.o

obj/viserion.o: src/viserion.c
	gcc -Wall -c -I include/ src/viserion.c -o obj/viserion.o

obj/rhaegal.o: src/rhaegal.c
	gcc -Wall -c -I include/ src/rhaegal.c -o obj/rhaegal.o

obj/crear_cache.o:src/crear_cache.c
	gcc -Wall -c -I include/ src/crear_cache.c -o obj/crear_cache.o

obj/destruir_cache.o:src/destruir_cache.c
	gcc -Wall -c -I include/ src/destruir_cache.c -o obj/destruir_cache.o

obj/devolver_cache.o:src/devolver_cache.c
	gcc -Wall -c -I include/ src/devolver_cache.c -o obj/devolver_cache.o

obj/obtener_cache.o:src/obtener_cache.c
	gcc -Wall -c -I include/ src/obtener_cache.c -o obj/obtener_cache.o

obj/stats_cache.o:src/stats_cache.c
	 gcc -Wall -c -I include/ src/stats_cache.c -o obj/stats_cache.o

run:
	./bin/prueba
#agregue las reglas que necesite


.PHONY: clean
clean:
	rm bin/* obj/*

